﻿#include <iostream>
using namespace std;


class Animal  // Base Class
{
public:
	Animal()
	{ }

	virtual void Voice()
	{
		cout << "Animal" << endl;
	}

	virtual ~Animal()
	{
		cout << "delete Base class Animal" << endl;
	}
};

class Dog : public Animal
{
public:
	Dog()
	{ }

	void Voice() override
	{
		cout << "Woof!" << endl;
	}

	~Dog()
	{
		cout << "delete Dog" << endl;
	}
};

class Cat : public Animal
{
public:
	Cat()
	{ }

	void Voice() override
	{
		cout << "Meow" << endl;
	}

	~Cat()
	{
		cout << "delete Cat" << endl;
	}
};

class Bear : public Animal
{
public:
	Bear()
	{ }

	void Voice() override
	{
		cout << "Roar!" << endl;
	}

	~Bear()
	{
		cout << "delete Bear" << endl;
	}
};


int main()
{
	Animal* animals[3] = { new Dog, new Cat, new Bear };

	for (int i = 0; i < 3; i++)
	{
		animals[i]->Voice();
		cout << endl;
	}

	delete animals[0]; cout << endl;
	delete animals[1]; cout << endl;
	delete animals[2]; cout << endl;

	return 0;
}
